## Training Backend Upplication

Welcome! This is an exercise for [backend developers](http://backend.upplication.com)
We hope you have fun and do your best :)

### How its work?

1. Fork this repo
2. Apply your solution
3. Review and... review again. Try to keep special attention on details.
4. Make a PR with a clear description and the time you spent solving it.
5. Wait for our response :)

Thanks for taking the time to take this little test with us.

### About this exercise

We have a spaghetti code in `com.upplication.training.solid.Main`. It works but we know we need to improve its
readability so we can make changes faster and avoid possible headaches....
We want you to apply [SOLID](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)) and make the code easier to test.

Apply your solution in about ~ 30 min.

That's all, *trust yourself* and try to balance your solution with the time you have.



